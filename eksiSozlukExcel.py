from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup  as soup
import xlsxwriter
from datetime import datetime
import sys
import time

headers = {
'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
address = "https://eksisozluk.com/hepsijet--6216184"

fileName = 'eksiSozluk.xlsx'
workbook = xlsxwriter.Workbook(fileName)
try:
    workbook.close()
except:
    print("\n" + fileName + " dosyası şu anda açık. Dosyayı kapatarak tekrar deneyin.")
    time.sleep(10)
    sys.exit(0)

workbook = xlsxwriter.Workbook(fileName)
pageNumStr = input("\nSayfa numarasını girin (Bütün sayfaların gelmesi için sıfır girin) : ")
pageNum = int(pageNumStr)

allPages = False
if (pageNum <= 0):
    allPages = True
    pageNum = 1

excelData = []
worksheet = workbook.add_worksheet()

morePages = True
while morePages:
    if (allPages == False):
        morePages = False
    site = address + "?p=" + str(pageNum)
    try:
        uClient = uReq(site)
        page_html = uClient.read()
        uClient.close()
        page_soup = soup(page_html, "html.parser")
        contents = page_soup.findAll("div", {"id": "topic"})
        headers = "content,info\n"
        for content in contents:
            contenthtml = content.findAll("div", {"class": "content"})
            if (len(contenthtml) == 0):
                morePages = False
            for i in range(len(contenthtml)):
                eksi_content = contenthtml[i].text
                contentinfo = content.findAll("div", {"class": "info"})
                eksi_info = contentinfo[i].text
                contentStr = str(eksi_content)
                contentStr = contentStr.lstrip()
                infoStr = str(eksi_info)
                infoStr = infoStr.lstrip()
                excelData.append([contentStr, infoStr])
        pageNum = pageNum + 1
    except:
        morePages = False

worksheet.set_column(0, 0, 20)
worksheet.set_column(1, 1, 20)
worksheet.set_column(2, 2, 25)
worksheet.set_column(3, 3, 140)
worksheet.set_row(0, 30)

bg_color = "#f7fff3";

cell_format_header = workbook.add_format()
cell_format_header.set_align('vcenter')
cell_format_header.set_bold()
cell_format_header.set_font_color('#e6ffe6')
cell_format_header.set_font_size(18)
cell_format_header.set_border(1)
cell_format_header.set_bg_color('#003300')

cell_format_message = workbook.add_format()
cell_format_message.set_text_wrap()
cell_format_message.set_align('top')
cell_format_message.set_border(1)
cell_format_message.set_bg_color(bg_color)

cell_format_date = workbook.add_format({'num_format': 'd mmmm yyyy hh:mm'})
date_format = workbook.add_format({'num_format': 'd mmmm yyyy hh:mm', 'align': 'left'})
date_format.set_align('top')
date_format.set_border(1)
date_format.set_bg_color(bg_color)

cell_format_writer = workbook.add_format()
cell_format_writer.set_text_wrap()
cell_format_writer.set_align('top')
cell_format_writer.set_border(1)
cell_format_writer.set_bg_color(bg_color)

col = 0
worksheet.write(0, col,     "Tarih", cell_format_header)
worksheet.write(0, col + 1, "Değiştirme", cell_format_header)
worksheet.write(0, col + 2, "Yazar", cell_format_header)
worksheet.write(0, col + 3, "Mesaj", cell_format_header)

row = 1
col = 0
for message, info in (excelData):
    date1 = info[:16]
    if(info[17] == '~'):
        if(info[21] == ':'):
            date2 = date1[:10] + info[18:24]
            writer = info[25:]
        else:
            date2 = info[19:35]
            writer = info[36:]
    else:
        date2 = ""
        writer = info[17:]
    date_time1 = datetime.strptime(date1,'%d.%m.%Y %H:%M')
    worksheet.write_datetime(row, col, date_time1, date_format)
    if(date2 != ''):
        date_time2 = datetime.strptime(date2,'%d.%m.%Y %H:%M')
        worksheet.write_datetime(row, col + 1, date_time2, date_format)
    else:
        worksheet.write(row, col + 1, "", cell_format_writer)
    worksheet.write(row, col + 2, writer, cell_format_writer)
    worksheet.write(row, col + 3, message, cell_format_message)
    row += 1

print("\nProgramın çalıştırıldığı yerde " + fileName + " isimli bir excel dosyasına veriler yazılmıştır.")
time.sleep(10)
workbook.close()